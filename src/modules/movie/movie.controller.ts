import { Controller, Get, Query } from '@nestjs/common';
import { MovieService } from './movie.service';

@Controller('movie')
export class MovieController {
    constructor(public readonly movieService: MovieService) {}

    @Get('movies')
    async getMovies(@Query('search') search: string) {
        if (search !== undefined && search.length > 1) {
            return await this.movieService.findMovies(search);
        }
    }


    @Get()
    async getData2() {
        return 'dfdff';
    }
}